%global tar_version 4.3.2

Name: python-ovirt-engine-sdk4
Summary: Python SDK for version 4 of the oVirt Engine API
Version: 4.3.2
Release: 2%{?dist}
Group: Development/Languages
License: ASL 2.0
URL: http://ovirt.org
Source: ovirt-engine-sdk-python-%{tar_version}.tar.gz

BuildRequires: gcc
BuildRequires: libxml2-devel
BuildRequires: python2-devel

Requires: libxml2
Requires: python
Requires: python-enum34
Requires: python-pycurl
Requires: python-six

%description
This package contains the Python SDK for version 4 of the oVirt Engine
API.

%prep
%setup -q -n ovirt-engine-sdk-python-%{tar_version}

%build
%py2_build

%install
%py2_install

%files
%doc README.adoc
%doc examples
%license LICENSE.txt
%{python2_sitearch}/*

%changelog
* Thu Mar 5 2020 di.wang <di.wang@cs2c.com.cn> - 4.3.2-2
- Package Initialization
